package com.example.fairly.demoapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private Button mBtnParse;
    private TextView mTxtParse;
    private JSONObject mJsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        String s = "In this tutorial, I'm going to show you how. Our app will have a simple and minimalist user " +
                "interface, showing the user exactly what they need to know about the current weather conditions. " +
                "Let's get started.";
        mJsonObject = new JSONObject();
        try {
            mJsonObject.put("KEY", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mJsonObject != null) {
            Log.d(TAG, mJsonObject.toString());
        }
        getViews();
        mBtnParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1 = null;
                try {
                    s1 = mJsonObject.getString("KEY");
                    Log.d(TAG, "s1 - " + s1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (s1 != null && !TextUtils.isEmpty(s1)) {
                    Log.d(TAG, "not null");
                    mTxtParse.setText(s1);
                }
            }
        });
    }

    private void getViews() {
        mBtnParse = (Button) findViewById(R.id.btnParse);
        mTxtParse = (TextView) findViewById(R.id.txtParse);
    }

    /*String toJSON(ArrayList<Domain> list) {
        Gson gson = new Gson();
        StringBuilder sb = new StringBuilder();
        for (Domain d : list) {
            sb.append(gson.toJson(d));
        }
        return sb.toString();
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
